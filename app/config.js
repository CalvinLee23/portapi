//config.js

/*
const config = {
  port: process.env.PORT || 8080,
  //This is the port that will be utilized on our local machines
  db: 'mongodb://localhost/myblog',
  test_port: 4242,
  //for testing purposes and will be used after create first route
  test_db: 'mongodb://localhost/myblog_test'
}

module.exports = config;
*/

// config.js
const config = {
  port: process.env.PORT || 8080,
  db: 'mongodb://localhost/myblog',
  test_port: 4242,
  test_db: 'mongodb://localhost/myblog_test'
}
module.exports = config;
